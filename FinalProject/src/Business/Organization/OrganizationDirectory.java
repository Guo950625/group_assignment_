/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type , String name){
        Organization organization = null;
        if (type.getValue().equals(Type.MaintenanceWorker.getValue())){
            organization = new MaintenanceWorkerOrganization();
            organization.setName(name);
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.MaintenanceSupplier.getValue())){
            organization = new MaintenceSupplierOrganization();
            organization.setName(name);
            organizationList.add(organization);
        }else if (type.getValue().equals(Type.SalesMan.getValue())){
            organization = new SalesManOrganization();
            organization.setName(name);
            organizationList.add(organization);
        }else if (type.getValue().equals(Type.SalesSupplier.getValue())){
            organization = new SalesSupplierOrganization();
            organization.setName(name);
            organizationList.add(organization);
        }else if (type.getValue().equals(Type.InsuranceWorker.getValue())){
            organization = new InsuranceWorkerOrganization();
            organization.setName(name);
            organizationList.add(organization);
        }
        return organization;
    }
}