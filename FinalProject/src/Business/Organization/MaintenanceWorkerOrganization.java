/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.MaintenanceWorkerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class MaintenanceWorkerOrganization extends Organization{

    public MaintenanceWorkerOrganization() {
        super(Organization.Type.MaintenanceWorker.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new MaintenanceWorkerRole());
        return roles;
    }
     
}