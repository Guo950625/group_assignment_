/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.SalesSupplierRole;
import java.util.ArrayList;

/**
 *
 * @author wgr
 */
public class SalesSupplierOrganization extends Organization{
    public SalesSupplierOrganization() {
        super(Organization.Type.SalesSupplier.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new SalesSupplierRole());
        return roles;
    }
}
