/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.MaintenanceAdminRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class MaintenanceAdminOrganization extends Organization{

    public MaintenanceAdminOrganization() {
        super(Type.MaintenanceAdmin.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new MaintenanceAdminRole());
        return roles;
    }
     
}
