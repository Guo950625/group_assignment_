/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Car.CarDirectory;
import Business.Employee.EmployeeDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;
import javax.swing.event.CaretListener;

/**
 *
 * @author wgr
 */
public abstract class Organization {

    private String name;
    private WorkQueue workQueue;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private CarDirectory carList;

    public CarDirectory getCarList() {
        return carList;
    }

    public void setCarList(CarDirectory carList) {
        this.carList = carList;
    }
    private int organizationID;
    private static int counter=0;
    
    public enum Type{
        MaintenanceAdmin("MaintenanceAdmin Organization"),
        MaintenanceWorker("MaintenanceWorker Organization"), 
        MaintenanceSupplier("MaintenanceSupplier Organization"),
        SalesAdmin("SalesAdmin Organization"),
        SalesMan("Salesman Organization"),
        SalesSupplier("SalesSupplier Organization"),
        InsuranceAdmin("InsuranceAdmin Organization"),
        InsuranceWorker("InsuranceWorker Organization");
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        carList = new CarDirectory();
        organizationID = counter;
        ++counter;
    }

    public abstract ArrayList<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
