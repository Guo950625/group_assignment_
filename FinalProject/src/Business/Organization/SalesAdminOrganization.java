/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import Business.Role.Role;
import Business.Role.SalesAdminRole;
import java.util.ArrayList;

/**
 *
 * @author wgr
 */
public class SalesAdminOrganization extends Organization{
    public SalesAdminOrganization() {
        super(Type.SalesAdmin.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new SalesAdminRole());
        return roles;
    }
}
