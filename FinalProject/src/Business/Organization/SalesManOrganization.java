/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Car.CarDirectory;
import Business.Role.Role;
import Business.Role.SalesManRole;
import java.util.ArrayList;

/**
 *
 * @author wgr
 */
public class SalesManOrganization extends Organization{   
    public SalesManOrganization() {
        super(Type.SalesMan.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new SalesManRole());
        return roles;
    }
}
