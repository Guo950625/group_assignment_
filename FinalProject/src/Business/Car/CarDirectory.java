/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Car;

import java.util.ArrayList;

/**
 *
 * @author wgr
 */
public class CarDirectory {
    private ArrayList<Car> carList;
    
    public CarDirectory(){
        this.carList = new ArrayList<Car>();
    }

    public ArrayList<Car> getCarList() {
        return carList;
    }

    public void setCarList(ArrayList<Car> carList) {
        this.carList = carList;
    }
    public Car createCar(String name, int quantity, int price){
        Car car = new Car();
        car.setName(name);
        car.setPrice(price);
        car.setQuantity(quantity);
        carList.add(car);
        return car;
    }
    
}
