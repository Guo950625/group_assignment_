/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luoqincheng
 */
public class CustomerDirectory {
    private List<Customer> customerlist;
    
    public CustomerDirectory(){
        customerlist = new ArrayList();
    }
    
     public void addCustomer(Customer cus){
        this.customerlist.add(cus);
    }
     
    public Customer authCustomer(String account,String password){
        if(customerlist==null){
            System.out.println("NULL LIST");
            return null;
        }
        for(Customer cus:customerlist){
            if(account.equals(cus.getAccount())&&password.equals(cus.getPassword()))
                return cus;
        }
        return null;
    }
    public List<Customer> getCustomerlist() {
        return customerlist;
    }

    public void setCustomerlist(List<Customer> customerlist) {
        this.customerlist = customerlist;
    }
     
     
}
