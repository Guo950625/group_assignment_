/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import Business.Car.CarDirectory;
import Business.WorkQueue.WorkQueue;

/**
 *
 * @author luoqincheng
 */
public class Customer {
    private String account;
    private String password;
    private String name;
    private WorkQueue workQueue;
    private CarDirectory carList;

    public CarDirectory getCarList() {
        return carList;
    }

    public void setCarList(CarDirectory carList) {
        this.carList = carList;
    }
    

    public Customer(String account,String password,String name){
        this.account=account;
        this.password=password;
        this.name=name;
        this.workQueue = new WorkQueue();
        this.carList = new CarDirectory();
        
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }
    public String getAccount() {
        return account;
    }

    public void setAccount(String Account) {
        this.account = Account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
   
    @Override
    public String toString() {
        return name;
    }
}
