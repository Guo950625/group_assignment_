/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Network.Network;
import Business.Network.NetworkDirectory;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import java.util.ArrayList;
import java.util.List;
import Business.Customer.Customer;
import Business.Customer.CustomerDirectory;

/**
 *
 * @author MyPC1
 */
public class EcoSystem extends Organization{
    
    private static EcoSystem business;
    private NetworkDirectory networkList;
    private CustomerDirectory cusdirectory;
    public static EcoSystem getInstance(){
        if(business==null){
            business=new EcoSystem();
        }
        return business;
    }
    
    public Network createAndAddNetwork(){
        Network network=new Network();
        networkList.getNetworkList().add(network);
        return network;
    }
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList=new ArrayList<Role>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }
    private EcoSystem(){
        super(null);
        networkList=new NetworkDirectory();
        cusdirectory=new CustomerDirectory();
    }

    public NetworkDirectory getNetworkList() {
        return networkList;
    }

    public void setNetworkList(NetworkDirectory networkList) {
        this.networkList = networkList;
    }


    public CustomerDirectory getCusdirectory() {
        return cusdirectory;
    }

    public void setCusdirectory(CustomerDirectory cusdirectory) {
        this.cusdirectory = cusdirectory;
    }

    
    
    public boolean checkIfUserIsUnique(String userName){
        if(!this.getUserAccountDirectory().checkIfUsernameIsUnique(userName)){
            return false;
        }
        for(Network network:networkList.getNetworkList()){
            
        }
        return true;
    }
}
