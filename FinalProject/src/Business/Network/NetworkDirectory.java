/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import java.util.ArrayList;

/**
 *
 * @author wgr
 */
public class NetworkDirectory {
    public ArrayList<Network> networkList;
    
    public NetworkDirectory(){
        this.networkList=new ArrayList<Network>();
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public void setNetworkList(ArrayList<Network> networkList) {
        this.networkList = networkList;
    }
    
    public Network createNetwork(String name){
        Network network = new Network();
        network.setName(name);
        networkList.add(network);
        return network;
    }
}
