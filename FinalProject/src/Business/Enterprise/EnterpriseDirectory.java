/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.OrganizationDirectory;
import java.util.ArrayList;

/**
 *
 * @author MyPC1
 */
public class EnterpriseDirectory {
    private ArrayList<Enterprise> enterpriseList;
   

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(ArrayList<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
    }
    
    public EnterpriseDirectory(){
        enterpriseList=new ArrayList<Enterprise>();
    }
    
    //Create enterprise
    public Enterprise createAndAddEnterprise(String name,Enterprise.EnterpriseType type){
        Enterprise enterprise=null;
         
        if(type.getValue().equals(Enterprise.EnterpriseType.Salesshop.getValue())){
            enterprise=new SaleshopEnterprise(name);
            enterpriseList.add(enterprise);
            System.out.println("1");
            
        }else if(type.getValue().equals(Enterprise.EnterpriseType.Maintenanceshop.getValue())){
            enterprise=new MaintenanceshopEnterprise(name);
            enterpriseList.add(enterprise);
            System.out.println("2");
        }else if(type.getValue().equals(Enterprise.EnterpriseType.Insurance.getValue())){
            enterprise=new InsuranceEnterprise(name);
            enterpriseList.add(enterprise);
            System.out.println("3");
        }
        return enterprise;
    }
}
