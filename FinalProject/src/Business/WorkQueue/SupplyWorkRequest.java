/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Car.Car;

/**
 *
 * @author raunak
 */
public class SupplyWorkRequest extends WorkRequest{
    
    private Car car;
    private int quantity;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    @Override
    public String toString(){
    return car.getName();
    }
    

    
    
    
}
