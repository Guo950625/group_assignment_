/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Car.Car;
import Business.UserAccount.UserAccount;
import Business.Customer.Customer;

/**
 *
 * @author luoqincheng
 */
public class CustomerRequest extends WorkRequest{
     private UserAccount receiver2;
     private String status2;
     private Customer cussender;
     private Car car;
     private int quantity;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public UserAccount getReceiver2() {
        return receiver2;
    }

    public void setReceiver2(UserAccount receiver2) {
        this.receiver2 = receiver2;
    }

    public String getStatus2() {
        return status2;
    }

    public void setStatus2(String status2) {
        this.status2 = status2;
    }

    public Customer getCussender() {
        return cussender;
    }

    public void setCussender(Customer cussender) {
        this.cussender = cussender;
    }
    
     @Override
    public String toString(){
        return car.getName();
    }
     
     
}
