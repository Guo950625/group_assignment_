/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class WorkQueue {
    
    private ArrayList<WorkRequest> workRequestList;
    private ArrayList<CustomerRequest> CustomerRequestList;
    private ArrayList<SupplyWorkRequest> SupplyWorkRequestsList;

    public WorkQueue() {
        workRequestList = new ArrayList();
        CustomerRequestList = new ArrayList();
        SupplyWorkRequestsList = new ArrayList();
    }

    public ArrayList<SupplyWorkRequest> getSupplyWorkRequestsList() {
        return SupplyWorkRequestsList;
    }

    public ArrayList<WorkRequest> getWorkRequestList() {
        return workRequestList;
    }
     public ArrayList<CustomerRequest> getCustomerRequestList() {
        return CustomerRequestList;
    }
}