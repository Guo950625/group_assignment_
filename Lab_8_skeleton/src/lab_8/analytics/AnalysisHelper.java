/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    
    
    public void userWithMostLikes(){
        Map<Integer,Integer> userLikecount = new HashMap<Integer,Integer>();
        Map<Integer,User> users = DataStore.getInstance().getUsers();
        for(User user: users.values()){
            for(Comment c:user.getComments()){
                int likes= 0;
                if(userLikecount.containsKey(user.getId()))
                    likes = userLikecount.get(user.getId());
                likes += c.getLikes();
                userLikecount.put(user.getId(), likes);           
            }
        }
        int max= 0;
        int maxId=0;
        for(int id:userLikecount.keySet()){
            if(userLikecount.get(id)>max){
                max = userLikecount.get(id);
                maxId = id;
            }
        }
        
        System.out.println("User with most likes:"+max+"\n"+users.get(maxId));
        
        
    }
    public void getFiveMostLikedComment(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        Collections.sort(commentList, new Comparator<Comment>(){
            @Override
            public int compare(Comment o1,Comment o2){
                return o2.getLikes()-o1.getLikes();
            }
        });
        
        System.out.println("5 most liked comments:");
        for(int i =0;i< commentList.size()&&i<5;i++){
            System.out.println(commentList.get(i));
        }
        
        
    }
    public void averageCommentLikes(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment>commentList = new ArrayList<>(comments.values());
        int sum = 0;
        for(Comment c:commentList){
            sum +=c.getLikes();  
        }
        
        System.out.println("Average number of likes per comment is "+ sum/commentList.size());
        
    }
    public void postWithMostLikes(){
        Map<Integer,Integer> postLikecount = new HashMap<Integer,Integer>();
        Map<Integer,Post> posts = DataStore.getInstance().getPosts();
        for(Post post:posts.values()){
            for(Comment com:post.getComments()){
                int like = 0;
                if(postLikecount.containsKey(post.getPostId()))
                    like= postLikecount.get(post.getPostId());
                    like += com.getLikes();
                    postLikecount.put(post.getPostId(), like);
                
            }
        }
        
        int max =0 ;
        int maxId = 0;
        for(int id:postLikecount.keySet()){
            if(postLikecount.get(id)>max){
                max = postLikecount.get(id);
                maxId = id;
            }
        }
        System.out.println("Post with most likes:"+max+"\n"+posts.get(maxId));
    }
    
    public void postWithMostComment(){
        Map<Integer,Integer> postCommentcount = new HashMap<Integer,Integer>();
        Map<Integer,Post> posts = DataStore.getInstance().getPosts();
        for(Post post:posts.values()){
            int count = 0;
            if(postCommentcount.containsKey(post.getPostId()))
                count = postCommentcount.get(post.getPostId());
                count = post.getComments().size();
                postCommentcount.put(post.getPostId(), count);
        }
        int max = 0;
        int maxId = 0;
        for(int id : postCommentcount.keySet()){
            if(postCommentcount.get(id)>max){
                max = postCommentcount.get(id);
                maxId = id;
            }
        }
       System.out.println("Post with most comments:"+max+"\n"+posts.get(maxId));
               
    }
    public void giveFivePostInactiveUsers(){
        Map<Integer,Post> posts = DataStore.getInstance().getPosts();
        Map<Integer,Integer> userPostcount = new HashMap<Integer,Integer>();
        Map<Integer,User> users = DataStore.getInstance().getUsers();
        
        for(Integer i : userPostcount.values()){
            for(User user:users.values()){
                userPostcount.put(user.getId(), 0);
            }
        }
        for(Post post:posts.values()){
            int count = 0;
            if(userPostcount.containsKey(post.getUserId()))
                count = userPostcount.get(post.getUserId());
                count += 1;
                userPostcount.put(post.getUserId(), count);
        }
        ArrayList<Integer> userCount = new ArrayList<>(userPostcount.values());
        ArrayList<User> userList = new ArrayList<>(users.values());
        for(int i=0;i<userCount.size();i++){
            userList.get(i).setPostNum(userCount.get(i));
        }
        Collections.sort(userList, new Comparator<User>(){
            @Override
            public int compare(User i1,User i2){
                return i2.getPostNum()-i1.getPostNum();
            }
        });
        
        System.out.println("Top 5 inactive users based on posts are:");
        for(int i = userList.size()-1;i>0&&i>userList.size()-6;i--){
            System.out.println(userList.get(i)+" Post: "+userList.get(i).getPostNum());
        }
    }
    
    public void giveFiveCommInactiveUsers(){
        Map<Integer,User>users = DataStore.getInstance().getUsers();
        List<User> userCount = new ArrayList<>(users.values());
        
        Collections.sort(userCount, new Comparator<User>(){
            @Override
            public int compare(User u1,User u2){
                return u2.getComments().size()-u1.getComments().size();
            }
        });
        
        System.out.println("Top 5 inactive users based on comments are:");
        for(int i =userCount.size()-1;i>0&&i>userCount.size()-6;i--){
            System.out.println(userCount.get(i));
        }
    }
    
    public void giveFiveAllInactiveUsers(){
        Map<Integer, User>users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> userPostcount = new HashMap<Integer,Integer>();
        
        for(Integer i : userPostcount.values()){
            for(User user:users.values()){
                userPostcount.put(user.getId(), 0);
            }
        }
        for(Post post:posts.values()){
            int count = 0;
            if(userPostcount.containsKey(post.getUserId()))
                count = userPostcount.get(post.getUserId());
                count += 1;
                userPostcount.put(post.getUserId(), count);
        }
        ArrayList<Integer> userCount = new ArrayList<>(userPostcount.values());
        ArrayList<User> userList = new ArrayList<>(users.values());
        for(int i=0;i<userCount.size();i++){
            userList.get(i).setPostNum(userCount.get(i));
        }
        
        for(User user:users.values()){
            int count = 0;
            for(Comment comment:user.getComments()){
                count+=comment.getLikes();
            }
            user.setLikeNum(count);
        }
        Collections.sort(userList, new Comparator<User>(){
            @Override
            public int compare(User u1,User u2){
                return u2.getComments().size()+u2.getPostNum()+u2.getLikeNum()-u1.getComments().size()-u1.getLikeNum()-u1.getPostNum();
            }
        });
        
        System.out.println("Top 5 inactive users based on comments, posts and likes are:");
        for(int i =userList.size()-1;i>0&&i>userList.size()-6;i--){
            int num = userList.get(i).getComments().size()+userList.get(i).getLikeNum()+userList.get(i).getPostNum();
            System.out.println(userList.get(i)+" The sum of comments, posts and likes are: "+num);
        }
        
        
        
    }
    public void giveFiveAllProactiveUsers(){
        Map<Integer, User>users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> userPostcount = new HashMap<Integer,Integer>();
        
        for(Integer i : userPostcount.values()){
            for(User user:users.values()){
                userPostcount.put(user.getId(), 0);
            }
        }
        for(Post post:posts.values()){
            int count = 0;
            if(userPostcount.containsKey(post.getUserId()))
                count = userPostcount.get(post.getUserId());
                count += 1;
                userPostcount.put(post.getUserId(), count);
        }
        ArrayList<Integer> userCount = new ArrayList<>(userPostcount.values());
        ArrayList<User> userList = new ArrayList<>(users.values());
        for(int i=0;i<userCount.size();i++){
            userList.get(i).setPostNum(userCount.get(i));
        }
        
        for(User user:users.values()){
            int count = 0;
            for(Comment comment:user.getComments()){
                count+=comment.getLikes();
            }
            user.setLikeNum(count);
        }
        Collections.sort(userList, new Comparator<User>(){
            @Override
            public int compare(User u1,User u2){
                return u2.getComments().size()+u2.getPostNum()+u2.getLikeNum()-u1.getComments().size()-u1.getLikeNum()-u1.getPostNum();
            }
        });
        
        System.out.println("Top 5 proactive users based on comments, posts and likes are:");
        for(int i =0;i<5&&i<userList.size();i++){
            int num = userList.get(i).getComments().size()+userList.get(i).getLikeNum()+userList.get(i).getPostNum();
            System.out.println(userList.get(i)+" The sum of comments, posts and likes are: "+num);
        }
        
        
        
    }
}
    

