/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author harshalneelkamal
 */
public class GateWay {
    
    public static void main(String args[]) throws IOException{
        
        DataGenerator generator = DataGenerator.getInstance();
        Analyses an=new Analyses();
        //Below is the sample for how you can use reader. you might wanna 
        //delete it once you understood.
        
        DataReader orderReader = new DataReader(generator.getOrderFilePath());
        String[] orderRow;
        while((orderRow = orderReader.getNextRow()) != null){
            int orderId = Integer.parseInt(orderRow[0]);
            int saleId = Integer.parseInt(orderRow[4]);
            int cusId = Integer.parseInt(orderRow[5]);
            int proId = Integer.parseInt(orderRow[2]);
            int proPrice = Integer.parseInt(orderRow[6]);
            int proQuantity = Integer.parseInt(orderRow[3]);
        
            Order order = new Order(orderId, saleId, cusId,new Item(proId,proPrice,proQuantity));
            DataStore.getInstance().getOrdermap().put(orderId,order);
        }

        DataReader productReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow;
        while((prodRow = productReader.getNextRow()) != null){
            int proId = Integer.parseInt(prodRow[0]);
            int minPrice = Integer.parseInt(prodRow[1]);
            int maxPrice = Integer.parseInt(prodRow[2]);
            int tarPrice = Integer.parseInt(prodRow[3]);
            Product product = new Product(proId,minPrice,maxPrice,tarPrice);
            DataStore.getInstance().getProductmap().put(proId, product);
             
        }
        an.TopThreePopular();
        an.ThreeBestCustomer();
        an.TopThreeSales();
        an.totalRevenue();
    }
    
    public static void printRow(String[] row){
        for (String row1 : row) {
            System.out.print(row1 + ", ");
        }
        System.out.println("");
    }
    
}
