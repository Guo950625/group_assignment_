/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.entities.Customer;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author luoqincheng & yixin guo
 */
public class Analyses {
    String[] top3pro=new String[3];
    public void TopThreePopular(){
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        Map<Integer,Order> order= DataStore.getInstance().getOrdermap();
        for(Order order1:order.values()){
            int salesNumber=0;
            if(map.containsKey(order1.getItem().getProductId()))
                salesNumber = map.get(order1.getItem().getProductId());
                salesNumber += order1.getItem().getQuantity();
                map.put(order1.getItem().getProductId(),salesNumber);
        }
        int max[]=new int[]{0,0,0};
        int maxId[]= new int[3];
        for(int id: map.keySet()){
            if(map.get(id)>max[0]){
            max[1]=max[0];
            max[2]=max[1];
            maxId[1]=maxId[0];
            maxId[2]=maxId[1];
            max[0]=map.get(id);
            maxId[0]=id;
            continue;
            }
            else if(map.get(id)>max[1]){
            max[2]=max[1];
            maxId[2]=maxId[1];
            max[1]=map.get(id);
            maxId[1]=id;
            continue;
            }
            else if(map.get(id)>max[2]){
            max[2]=map.get(id);
            maxId[2]=id;
            }
            }
        System.out.println("Top3 most popular product sorted from high to low:");
        System.out.println("1.productid:"+maxId[0]+"  SalseNumber:"+max[0]);
        System.out.println("2.productid:"+maxId[1]+"  SalseNumber:"+max[1]);
        System.out.println("3.productid:"+maxId[2]+"  SalseNumber:"+max[2]);
    }
   public void ThreeBestCustomer(){
        Map<Integer,Order> order = DataStore.getInstance().getOrdermap();
        Map<Integer, Integer> map = new HashMap<Integer,Integer>();
            for(Order o: order.values()){
                int expense = 0;
                if(map.containsKey(o.getCustomerId()))
                    expense = map.get(o.getCustomerId());
                    expense += o.getItem().getSalesPrice()*o.getItem().getQuantity();
                    map.put(o.getCustomerId(), expense);
            
        }
        int max[]=new int[]{0,0,0};
        int maxId[]= new int[3];
        for(int id: map.keySet()){
            if(map.get(id)>max[0]){
            max[1]=max[0];
            max[2]=max[1];
            maxId[1]=maxId[0];
            maxId[2]=maxId[1];
            max[0]=map.get(id);
            maxId[0]=id;
            continue;
            }
            else if(map.get(id)>max[1]){
            max[2]=max[1];
            maxId[2]=maxId[1];
            max[1]=map.get(id);
            maxId[1]=id;
            continue;
            }
            else if(map.get(id)>max[2]){
            max[2]=map.get(id);
            maxId[2]=id;
            }
            }
        System.out.println("Top3 best customers:");
        System.out.println("1.customerid:"+maxId[0]+"  expense:"+max[0]);
        System.out.println("2.customerid:"+maxId[1]+"  expense:"+max[1]);
        System.out.println("3.customerid:"+maxId[2]+"  expense:"+max[2]);
    }
     public void TopThreeSales(){
        Map<Integer, Integer> idAndMoney = new HashMap<Integer, Integer>();
        Map<Integer,Order> order= DataStore.getInstance().getOrdermap();
        Map<Integer,Product> product= DataStore.getInstance().getProductmap();
         for(Order order1:order.values()){
            int money=0;
            if(idAndMoney.containsKey(order1.getSalesId()))
                money = idAndMoney.get(order1.getSalesId());
                money += (order1.getItem().getQuantity()*(order1.getItem().getSalesPrice()-product.get(order1.getItem().getProductId()).getMinPrice()));
                //System.out.println(money);
                idAndMoney.put(order1.getSalesId(),money);
        }
        int max[]=new int[]{0,0,0};
        int maxId[]= new int[3];
         for(int id: idAndMoney.keySet()){
            if(idAndMoney.get(id)>max[0]){
            max[2]=max[1];
            max[1]=max[0];
            maxId[2]=maxId[1];
            maxId[1]=maxId[0];
            max[0]=idAndMoney.get(id);
            maxId[0]=id;
            continue;
            }
            else if(idAndMoney.get(id)>max[1]){
            max[2]=max[1];
            maxId[2]=maxId[1];
            max[1]=idAndMoney.get(id);
            maxId[1]=id;
            continue;
            }
            else if(idAndMoney.get(id)>max[2]){
            max[2]=idAndMoney.get(id);
            maxId[2]=id;
            }
            }
        System.out.println("Top3 best sales sorted from high to low:");
        System.out.println("1.salesid:"+maxId[0]+"  sales amount:"+max[0]);
        System.out.println("2.salesid:"+maxId[1]+"  sales amount:"+max[1]);
        System.out.println("3.salesid:"+maxId[2]+"  sales amount:"+max[2]);
    }
     public void totalRevenue(){
        Map<Integer,Order> order= DataStore.getInstance().getOrdermap();
        Map<Integer,Product> product= DataStore.getInstance().getProductmap();
         int totalMoney=0;
        
         for(Order order1:order.values()){
           totalMoney += (order1.getItem().getQuantity()*(order1.getItem().getSalesPrice()-product.get( order1.getItem().getProductId()).getMinPrice()));
        }
        System.out.println("Our total revenue for the year: ");
        System.out.println(totalMoney);
    }

}
