/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.entities.Customer;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author luoqincheng
 */
public class DataStore {
     private static DataStore dataStore;
    
    private Map<Integer, Order> ordermap;
     private Map<Integer, Customer> customermap;
      private Map<Integer, SalesPerson> salespersonmap;
       private Map<Integer, Product> productmap;
    
    private DataStore(){
        ordermap = new HashMap<>();
        customermap= new HashMap<>();
        salespersonmap= new HashMap<>();
         productmap= new HashMap<>();
    }
    
    public static DataStore getInstance(){
        if(dataStore == null)
            dataStore = new DataStore();
        return dataStore;
    }

    public static DataStore getDataStore() {
        return dataStore;
    }

    public static void setDataStore(DataStore dataStore) {
        DataStore.dataStore = dataStore;
    }

    public Map<Integer, Order> getOrdermap() {
        return ordermap;
    }

    public void setOrdermap(Map<Integer, Order> ordermap) {
        this.ordermap = ordermap;
    }

    public Map<Integer, Customer> getCustomermap() {
        return customermap;
    }

    public void setCustomermap(Map<Integer, Customer> customermap) {
        this.customermap = customermap;
    }

    public Map<Integer, SalesPerson> getSalespersonmap() {
        return salespersonmap;
    }

    public void setSalespersonmap(Map<Integer, SalesPerson> salespersonmap) {
        this.salespersonmap = salespersonmap;
    }

    public Map<Integer, Product> getProductmap() {
        return productmap;
    }

    public void setProductmap(Map<Integer, Product> productmap) {
        this.productmap = productmap;
    }

    
}
